package spring.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import spring.model.Person

interface PersonRepository:JpaRepository<Person,Long>
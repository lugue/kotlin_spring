package spring.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.validation.constraints.NotBlank

@Entity
class Person(
    @Id
    @GeneratedValue
    var id: Long? = null,
    @field:NotBlank(message = "must have a name")
    var name: String? = ""
)
package spring.exception

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import javax.persistence.EntityNotFoundException

@RestControllerAdvice
class ExceptionHandler {

    @ExceptionHandler(EntityNotFoundException::class)
    fun notFound(ex: EntityNotFoundException): ResponseEntity<ApiError> =
        ApiError(ex.javaClass.canonicalName, ex.message, HttpStatus.NOT_FOUND).toResponseEntity()
}
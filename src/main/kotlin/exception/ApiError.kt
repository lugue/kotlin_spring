package spring.exception

import com.fasterxml.jackson.annotation.JsonFormat
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import java.time.LocalDateTime

data class ApiError(
    val error: String?,
    val message: String?,
    val status: HttpStatus
) {
    @JsonFormat
        (shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy hh:mm:ss")
    val time: LocalDateTime? = LocalDateTime.now()
    val code: Int = status.value()

    fun toResponseEntity():ResponseEntity<ApiError> = ResponseEntity(this,status)

}
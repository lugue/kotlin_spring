package spring.controller

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import spring.model.Person
import spring.repository.PersonRepository
import javax.validation.Valid

@RestController
@RequestMapping("persons")
class PersonController(private val repo: PersonRepository) {
    
    @PostMapping
    fun save(@Valid @RequestBody p: Person): ResponseEntity<Person> = ResponseEntity.ok(repo.save(p))

    @GetMapping("{id}")
    fun find(@PathVariable id: Long): ResponseEntity<Person> = ResponseEntity.ok(repo.getOne(id))

}